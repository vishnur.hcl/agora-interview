# Complete Solution 

1. Login to gitlab portal.
   Created below repositories. Docker images details see on step 4

| Name                | How to get                                     | Description             |
|---------------------|------------------------------------------------|-------------------------|
| `agora-interview`   | https://gitlab.com/vishnur.hcl/agora-interview | Application stack       |                                                                                                                                                                                                                                                            
 | `agora-terraform`     | https://gitlab.com/vishnur.hcl/agora-terraform | Infra stack             |
 | `vishnur1/myregistry`  | docker pull vishnur1/myregistry:tagname        | Docker images on docker registry |

2. Configure a gitlab runner on a VM . I followed below command on my local Linux machine
   ``` 
   # Download the binary for your system
   sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

   # Give it permission to execute
   sudo chmod +x /usr/local/bin/gitlab-runner

   # Create a GitLab Runner user
   sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

   # Install and run as a service
   sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
   sudo gitlab-runner start
   
   #Command to register runner
   sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
   
   ```
   Check your runner is working fine
   Repository -> Setting -> CICD -> Runners  .It should appear in green as `Available Specific Runner`.
   We can also use  docker or kubernet executor for gitlab runner.
   https://docs.gitlab.com/runner/install/kubernetes.html . Gitlab also provides shared runners but that
   need your credit card details. 
  
3. Run below commands on your Gitlab runner machine
   ```
   #terraform Installation
   curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
   sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
   sudo apt-get update && sudo apt-get install terraform  
   
   #AWS key adding to run terraform
   export AWS_ACCESS_KEY_ID="<your access key id>"
   export AWS_SECRET_ACCESS_KEY="<your access key>"
   
   #kubectl installation
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
   sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
   
   #I faced some issues with Docker  Container to build docker image ,so I installed Docker also to build image
   # This link will help you to do that https://docs.docker.com/engine/install/ubuntu/
   # This is required to create docker images for the app.py and to upload to docker registry
   ```
4. Create account on Docker Registry 
   https://hub.docker.com/
   Created token  for docker push and pull
   Setting -> security 

5. Create variable PASSWD in Setting -> CICD -> Variables
   PASSWD should contain above (step 4) token 

6. Install Helm since I uses helm as package manager

   ``` 
   curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
   sudo apt-get install apt-transport-https --yes
   echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
   sudo apt-get update
   sudo apt-get install helm
   ```

7. Run pipeline for `agora-terraform`  CICD-> Pipelines. Check your EKS cluster got deployed 
   
8. Run pipeline for `agora-interview` CICD-> Pipelines. Check your Application got deployed.
   Working Pipeline will look like this
   https://gitlab.com/vishnur.hcl/agora-interview/-/pipelines/510443047

9. After all this ,it will be CICD enabled which means Whenever you commit/push your changes ,it should  create new
   version of the App . Terraform repo also help you to update your changes in AWS 


##agora-interview 

This module actually create everything for Application Deployment
1. `.gitlab-ci.yml` which will create all pipeline jobs
2. `app.py` contains python application 
3. `flask-depend.txt` will have flask module dependencies for docker image
4. `Dockerfile` file create docker image
5. `deploy` Directory contains Helm package details along 
6. I used Docker registry for Image upload and download
